# Hosts

Sets up the /etc/hosts file with:

* custom entries,
* ad/spyware blocking.

# Custom Entries

Create files in your `personal` cookbook under `files/hosts`.
The content of these files will be merged into you /etc/hosts.

# Blocking

The GitHub project StevenBlack/hosts is used to populate the hosts file
with entries which map undesirable domains to `0.0.0.0` meaning that requests
to those hosts results in a filaing local call.

## Customization

Sometimes, you may wish to unblock certain entries in your hosts file,
example when you are working with Google Analytics.

You can include an `unblock` section in your attributes, e.g.:

```json
  "hosts": {
    "unblock": [
      "analytics\\.google\\.com"
    ]
  }
```

N.B.: entries are regular expressions. All matching lines in the
StevenBlack/hosts repo will be commented out in your /etc/hosts.

