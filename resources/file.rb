resource_name :hosts_file

actions :create
default_action :create

property :name, String, default: "hosts"
property :repo, String
property :commit, String
property :checksum, String
property :extras, Hash, default: {}
property :unblock, Array, default: []

action_class do
  include Hosts::Helpers
end

action :create do
  remote_file raw_hosts_path do
    source hosts_source
    checksum new_resource.checksum
    notifies :create, "file[#{hosts_path}]", :immediately
  end

  file hosts_path do
    # using `lazy` as we'll only have content in the execution phase
    content lazy { prepare_hosts }
  end
end
