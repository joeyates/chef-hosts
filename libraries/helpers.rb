module Hosts
  module Helpers
    def whyrun_supported?
      true
    end

    def extra_lines
      new_resource.extras.map { |ip, host| "#{ip} #{host}\n" }.join
    end

    def prepare_hosts
      # Skip during why-run
      return "" if !::File.exist?(raw_hosts_path)

      raw_hosts_content = ::File.read(raw_hosts_path)

      add_extras(unblock_some(raw_hosts_content))
    end

    def add_extras(hosts)
      h = hosts.dup
      # Insert static files between markers
      # The markers are assumed to exist in the downloaded raw hosts file.
      # If the pre- and post-markers are not found,
      # this fails with "IndexError: regexp not matched"
      pre = "# Custom host records are listed here."
      post = "# End of custom host records."
      replacement = pre + "\n" + extra_lines + "\n" + post
      h[/#{Regexp.escape(pre)}.*?#{Regexp.escape(post)}/m] = replacement
      h
    end

    def unblock_some(hosts)
      h = hosts.dup
      new_resource.unblock.each do |unblock|
        h.gsub!(/^0\.0\.0\.0\s+(\S+\.|)#{unblock}$/m) do |match|
          "# #{match} # unblocked domain '#{unblock}'"
        end
      end
      h
    end

    def hosts_source
      "#{new_resource.repo}/#{new_resource.commit}/hosts"
    end

    def system_config_path
      case node["platform_family"]
      when "mac_os_x"
        "/private/etc"
      else
        "/etc"
      end
    end

    def hosts_path
      ::File.join(system_config_path, "hosts")
    end

    def raw_hosts_path
      ::File.join(system_config_path, "hosts.raw")
    end
  end
end
