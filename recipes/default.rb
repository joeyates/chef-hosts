hosts_file do
  repo node["hosts"]["repo"]
  commit node["hosts"]["commit"]
  checksum node["hosts"]["checksum"]
  extras node["hosts"]["extras"]
  unblock node["hosts"]["unblock"]
end
