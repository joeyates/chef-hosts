default["hosts"]["repo"] = "https://raw.githubusercontent.com/StevenBlack/hosts"
default["hosts"]["commit"] = "158b9b5e814a260c822eb67e31baa043e8d1252f"
default["hosts"]["checksum"] = "174dea75e3f0d29a757d05fa665b1a29bce5c1556d308a7a22f0f3a0044380bf"
default["hosts"]["extras"] = {}
default["hosts"]["unblock"] = []
