name             "hosts"
maintainer       "Joe Yates"
maintainer_email "joe.g.yates@gmail.com"
license          "All rights reserved"
description      "Installs/Configures /etc/hosts"
long_description IO.read(File.join(File.dirname(__FILE__), "README.md"))
version          "0.2.0"
